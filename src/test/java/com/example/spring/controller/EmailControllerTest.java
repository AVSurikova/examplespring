package com.example.spring.controller;

import com.example.spring.model.Employee;
import com.example.spring.repository.EmployeeRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class EmailControllerTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Rule
    public SmtpServerRule smtpServerRule = new SmtpServerRule(587);

    @Test
    public void testSendSimpleEmailOk() {
        Employee employee = createTestEmployee("test@mail.ru");
        ResponseEntity<String> response = restTemplate.exchange("/email/{id}",
                HttpMethod.GET, null, String.class, employee.getId());

        assertEquals("Please check your inbox", response.getBody());
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        System.out.println(Arrays.toString(receivedMessages));
    }

    @Test
    public void testSendSimpleEmailInvalidId() {
        ResponseEntity<String> response = restTemplate.exchange("/email/{id}",
                HttpMethod.GET, null, String.class, 1);

        assertEquals("Unable to send email - invalid id", response.getBody());
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testSimpleEmailInvalidEmail() {
        Employee employee = createTestEmployee("6r56r19r619");
        ResponseEntity<String> response = restTemplate.exchange("/email/{id}",
                HttpMethod.GET, null, String.class, employee.getId());

        assertEquals("Unable to send email - invalid mail", response.getBody());
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private Employee createTestEmployee(String email) {
        Employee employee = new Employee();
        employee.setName("Test");
        employee.setEmail(email);

        return employeeRepository.save(employee);
    }
}
