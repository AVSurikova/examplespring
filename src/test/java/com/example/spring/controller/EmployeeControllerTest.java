package com.example.spring.controller;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.mapper.EmployeeMapper;
import com.example.spring.repository.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EmployeeControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    EmployeeMapper employeeMapper;

    @AfterEach
    public void resetDb() {
        employeeRepository.deleteAll();
    }

    @Test
    public void testCreate() {
        EmployeeDTO employee = new EmployeeDTO();
        employee.setName("Test");
        employee.setEmail("test@mail.ru");

        ResponseEntity<EmployeeDTO> response = restTemplate.postForEntity("/employee", employee, EmployeeDTO.class);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(Objects.requireNonNull(response.getBody()).getId(), notNullValue());
        assertThat(response.getBody().getName(), is(employee.getName()));
        assertThat(response.getBody().getEmail(), is(employee.getEmail()));
    }

    @Test
    public void testFindAll() {
        createTestEmployee("Test1", "test1@mail.ru");
        createTestEmployee("Test2", "test2@mail.ru");

        ResponseEntity<List<EmployeeDTO>> response = restTemplate.exchange("/employee", HttpMethod.GET,
                null, new ParameterizedTypeReference<>() {});

        List<EmployeeDTO> employees = response.getBody();

        assertThat(employees, hasSize(2));
        assertThat(employees.get(0).getName(), is("Test1"));
        assertThat(employees.get(1).getName(), is("Test2"));
    }

    @Test
    public void testFindById() {
        long id = createTestEmployee("Test", "test@mail.ru").getId();

        EmployeeDTO employeeDTO = restTemplate.getForObject("/employee/{id}", EmployeeDTO.class, id);
        assertThat(employeeDTO.getName(), is("Test"));
        assertThat(employeeDTO.getEmail(), is("test@mail.ru"));
    }

    @Test
    public void testUpdate() {
        long id = createTestEmployee("Test", "test@mail.ru").getId();
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName("testUp");
        employeeDTO.setEmail("testUp@mail.ru");

        HttpEntity<EmployeeDTO> entity = new HttpEntity<>(employeeDTO);
        ResponseEntity<EmployeeDTO> response = restTemplate.exchange("/employee/{id}",
                HttpMethod.PUT, entity, EmployeeDTO.class, id);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(Objects.requireNonNull(response.getBody()).getId(), notNullValue());
        assertThat(response.getBody().getName(), is(employeeDTO.getName()));
        assertThat(response.getBody().getEmail(), is(employeeDTO.getEmail()));
    }

    @Test
    public void testDelete() {
        long id = createTestEmployee("Test", "test@mail.ru").getId();

        ResponseEntity<String> response = restTemplate.exchange("/employee/{id}", HttpMethod.DELETE,
                null, String.class, id);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody(), is("Successfully"));

    }

    private EmployeeDTO createTestEmployee(String name, String email) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName(name);
        employeeDTO.setEmail(email);

        return employeeMapper.toEmployeeDTO(employeeRepository.save(employeeMapper.toEmployee(employeeDTO)));
    }

}
