package com.example.spring.service;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.mapper.EmployeeMapper;
import com.example.spring.repository.EmployeeRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EmployeeServiceTest {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeMapper employeeMapper;

    @After
    public void resetDb() {
        employeeRepository.deleteAll();
    }

    @Test
    public void testCreate() {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName("Test");
        employeeDTO.setEmail("test@mail.ru");
        EmployeeDTO employeeDTONew = employeeService.create(employeeDTO);

        assertTrue(employeeDTONew.getId() != 0);
        assertEquals(employeeDTONew.getName(), employeeDTO.getName());
        assertEquals(employeeDTONew.getEmail(), employeeDTO.getEmail());
    }

    @Test
    public void testFindAll() {
        EmployeeDTO employeeDTO1 = createTestEmployee("Test1", "test1@mail.ru");
        EmployeeDTO employeeDTO2 = createTestEmployee("Test2", "test2@mail.ru");
        List<EmployeeDTO> employees = employeeService.findAll();

        assertEquals(2, employees.size());
        assertEquals(employees.get(0).getId(), employeeDTO1.getId());
        assertEquals(employees.get(0).getName(), employeeDTO1.getName());
        assertEquals(employees.get(0).getEmail(), employeeDTO1.getEmail());
        assertEquals(employees.get(1).getId(), employeeDTO2.getId());
        assertEquals(employees.get(1).getName(), employeeDTO2.getName());
        assertEquals(employees.get(1).getEmail(), employeeDTO2.getEmail());
    }

    @Test
    public void testFindById() {
        EmployeeDTO employeeDTO = createTestEmployee("Test", "test@mail.ru");
        EmployeeDTO employeeDTONew = employeeService.findById(employeeDTO.getId());

        assertEquals(employeeDTONew.getId(), employeeDTO.getId());
        assertEquals(employeeDTONew.getName(), employeeDTO.getName());
        assertEquals(employeeDTONew.getEmail(), employeeDTO.getEmail());
    }

    @Test
    public void testUpdate() {
        EmployeeDTO employeeDTO = createTestEmployee("Test", "test@mail.ru");
        EmployeeDTO employeeDTOUp = new EmployeeDTO();
        employeeDTOUp.setName("TestUp");
        employeeDTOUp.setEmail("testup@mail.ru");
        EmployeeDTO employeeDTONew = employeeService.update(employeeDTO.getId(), employeeDTOUp);

        assertTrue(employeeDTONew.getId() != 0);
        assertEquals(employeeDTONew.getName(), employeeDTOUp.getName());
        assertEquals(employeeDTONew.getEmail(), employeeDTOUp.getEmail());
    }

    @Test
    public void delete() {
        EmployeeDTO employeeDTO = createTestEmployee("Test", "test@mail.ru");
        employeeService.delete(employeeDTO.getId());

        assertEquals(0, employeeService.findAll().size());
    }

    private EmployeeDTO createTestEmployee(String name, String email) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setName(name);
        employeeDTO.setEmail(email);

        return employeeMapper.toEmployeeDTO(employeeRepository.save(employeeMapper.toEmployee(employeeDTO)));
    }
}
