package com.example.spring.service;

import com.example.spring.Application;
import org.awaitility.Duration;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;

@SpringBootTest
@SpringJUnitConfig(Application.class)
public class SchedulerServiceTest {
    @SpyBean
    private SchedulerService schedulerService;

    @Test
    public void test() {
        await().atMost(Duration.ONE_SECOND)
                .untilAsserted(() -> verify(schedulerService, atLeast(0)).sentEmail());
    }
}
