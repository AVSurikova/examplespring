package com.example.spring.mapper;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@JsonTest
@RunWith(SpringRunner.class)
public class EmployeeMapperTest {
    @Autowired
    private JacksonTester<EmployeeDTO> json;
    private final EmployeeMapper employeeMapper = new EmployeeMapper();

    @Test
    public void testToEmployeeDTO() {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("Test");
        employee.setEmail("test@mail.ru");
        EmployeeDTO employeeDTO = employeeMapper.toEmployeeDTO(employee);

        assertEquals(employee.getId(), employeeDTO.getId());
        assertEquals(employee.getName(), employeeDTO.getName());
        assertEquals(employee.getEmail(), employeeDTO.getEmail());
    }

    @Test
    public void testToEmployee() {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setId(1);
        employeeDTO.setName("Test");
        employeeDTO.setEmail("test@mail.ru");
        Employee employee = employeeMapper.toEmployee(employeeDTO);

        assertEquals(employeeDTO.getId(), employee.getId());
        assertEquals(employeeDTO.getName(), employee.getName());
        assertEquals(employeeDTO.getEmail(), employee.getEmail());
    }

    @Test
    public void testSerializeEmployeeDTO() throws Exception {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("Test");
        employee.setEmail("test@mail.ru");
        EmployeeDTO employeeDTO = employeeMapper.toEmployeeDTO(employee);
        JsonContent<EmployeeDTO> result = json.write(employeeDTO);

        assertThat(result).hasJsonPathStringValue("$.name");
        assertThat(result).extractingJsonPathStringValue("$.name").isEqualTo(employeeDTO.getName());
        assertThat(result).extractingJsonPathStringValue("$.email").isEqualTo(employeeDTO.getEmail());
        assertThat(result).doesNotHaveJsonPath("$.enabled");
    }

    @Test
    public void testDeserializeEmployeeDTO() throws Exception {
        String jsonContent = "{\"name\": \"Test\", \"email\": \"test@mail.ru\"}";
        EmployeeDTO employeeDTO = json.parse(jsonContent).getObject();
        Employee employee = employeeMapper.toEmployee(employeeDTO);

        assertThat(employeeDTO.getName()).isEqualTo("Test");
        assertThat(employeeDTO.getEmail()).isEqualTo("test@mail.ru");
        assertThat(employee.getId()).isEqualTo(employeeDTO.getId());
        assertThat(employee.getName()).isEqualTo(employeeDTO.getName());
        assertThat(employee.getEmail()).isEqualTo(employeeDTO.getEmail());
    }
}
