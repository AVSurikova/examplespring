package com.example.spring.controller;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    public ResponseEntity<EmployeeDTO> create(@RequestBody EmployeeDTO employee) {
        EmployeeDTO employeeCreate = employeeService.create(employee);

        return new ResponseEntity<>(employeeCreate, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<EmployeeDTO>> findAll() {
        List<EmployeeDTO> employees = employeeService.findAll();

        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EmployeeDTO> findById(@PathVariable(name = "id") long id) {
        EmployeeDTO employee = employeeService.findById(id);

        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = "application/json",  consumes = "application/json")
    public ResponseEntity<EmployeeDTO> update(@PathVariable(name = "id") long id, @RequestBody EmployeeDTO employee) {
        EmployeeDTO employeeUpdate = employeeService.update(id, employee);

        return new ResponseEntity<>(employeeUpdate, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") long id) {
        employeeService.delete(id);

        return new ResponseEntity<>("Successfully", HttpStatus.OK);
    }
}
