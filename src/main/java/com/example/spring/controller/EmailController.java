package com.example.spring.controller;

import com.example.spring.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/email")
public class EmailController {
    private final EmailService emailService;

    @Autowired
    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping(value = "/hello")
    public ResponseEntity<String> sayHello() {
        return new ResponseEntity<>("Hello", HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<String> sendSimpleEmail(@PathVariable(name = "id") long id) {
        try {
            emailService.sendEmail(id, "Tea break", "It's time to drink tea!");
        } catch (NullPointerException exception) {
            return new ResponseEntity<>("Unable to send email - invalid id", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (MailException e) {
            return new ResponseEntity<>("Unable to send email - invalid mail", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            return new ResponseEntity<>("Unable to send email", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Please check your inbox", HttpStatus.OK);
    }
}
