package com.example.spring.service;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    private final JavaMailSender emailSender;
    private final EmployeeService employeeService;

    @Autowired
    public EmailService(JavaMailSender emailSender, EmployeeService employeeService) {
        this.emailSender = emailSender;
        this.employeeService = employeeService;
    }

    public void sendEmail(long id, String subject, String message) throws NullPointerException {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        EmployeeDTO employee = employeeService.findById(id);

        simpleMailMessage.setTo(employee.getEmail());
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);

        emailSender.send(simpleMailMessage);
    }

    public void sendTemplateEmail(Employee employee) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setTo(employee.getEmail());
        simpleMailMessage.setSubject("Tea break");
        simpleMailMessage.setText("It's time to drink tea!");

        emailSender.send(simpleMailMessage);
    }
}
