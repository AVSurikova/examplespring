package com.example.spring.service;

import com.example.spring.dto.EmployeeDTO;
import com.example.spring.mapper.EmployeeMapper;
import com.example.spring.model.Employee;
import com.example.spring.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    public EmployeeDTO create(EmployeeDTO employeeDTO) {
        Employee employee = employeeMapper.toEmployee(employeeDTO);
        employeeRepository.save(employee);

        return employeeMapper.toEmployeeDTO(employee);
    }

    public List<EmployeeDTO> findAll() {
        List<Employee> employees = (List<Employee>) employeeRepository.findAll();

        return employees.stream().map(employeeMapper::toEmployeeDTO).collect(Collectors.toList());
    }

    public EmployeeDTO findById(long id) throws NullPointerException {
        Employee employee = employeeRepository.findById(id).orElse(new Employee());

        return employeeMapper.toEmployeeDTO(employee);
    }

    public EmployeeDTO update(long id, EmployeeDTO employeeDTO) {
        Employee employee = employeeMapper.toEmployee(employeeDTO);

        employee.setId(id);
        employeeRepository.save(employee);

        return employeeMapper.toEmployeeDTO(employee);
    }

    public void delete(long id) {
        employeeRepository.deleteById(id);
    }
}
