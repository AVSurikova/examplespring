package com.example.spring.service;

import com.example.spring.mapper.EmployeeMapper;
import com.example.spring.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SchedulerService {
    private final EmailService emailService;
    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    @Autowired
    public SchedulerService(EmailService emailService, EmployeeService employeeService, EmployeeMapper employeeMapper) {
        this.emailService = emailService;
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
    }

    @Scheduled(cron = "0 0 17 ? * MON-FRI")
    public void sentEmail() {
        List<Employee> employees = employeeService.findAll().stream().map(employeeMapper::toEmployee).collect(Collectors.toList());

        for (Employee employee : employees) {
            emailService.sendTemplateEmail(employee);
        }
    }
}
